#!/bin/bash
set -e
replicated_filesystem=tank/replicated
snap_attribute='j.v:sent'
function snapshots
{
  zfs list -t snapshot -o $snap_attribute,name -H -s creation | grep "$root_filesystem@"
}

function zfs_receive
{
  ssh zfs-slave -- zfs receive tank/backup
}

function abort_if_empty {
if [[ -z ${!1} ]]; then
  echo "$2" 1>&2
  exit 1
fi
}

last_sent=$(snapshots | awk '/^1/ { print $2 }' | tail -1 | sed 's/[^@]*//')
abort_if_empty last_sent 'Could not find last sent snapshot'
echo Last sent: $last_sent
send_snapshot=$(snapshots | awk '/^-/ { print $2 }' | tail -1)
abort_if_empty send_snapshot 'Could not find snapshot to send'
echo Snapshot to send: $send_snapshot
zfs send -RI $last_sent $send_snapshot | zfs_receive
zfs set $snap_attribute=1 $send_snapshot
